" Description:	Vinscripts configuration
" Maintainer:	DALECKI Léo
" License:	MIT License
" Home:		<https://framagit.org/Informancien/vinscripts>

let s:cpo_bck = &cpoptions
set cpoptions&vim

let s:defaults = {
	\ 'vimwiki_dot': {
		\ 'script_start': '{{{dot',
		\ 'script_end': '}}}',
		\ 'script_attr': 'hidden',
		\ 'output_start': '{{{graph',
		\ 'output_end': '}}}',
		\ 'output_before': 1,
		\ 'bin': 'graph-easy --from_graphviz',
		\ 'bin_encoding':
			\ {'utf-8': 'graph-easy --from_graphviz --boxart'},
		\ 'filetype': 'wiki' },
	\ 'vimwiki_graph-easy': {
		\ 'script_start': '{{{graph-easy',
		\ 'script_end': '}}}',
		\ 'script_attr': 'hidden',
		\ 'output_start': '{{{graph',
		\ 'output_end': '}}}',
		\ 'output_before': 1,
		\ 'bin': 'graph-easy --from_txt',
		\ 'bin_encoding':
			\ {'utf-8': 'graph-easy --from_txt --boxart'},
		\ 'filetype': 'wiki' },
\ }

function! vinscripts#conf#defaults()
	if type(g:vinscripts_defaults) == v:t_number
			\ && g:vinscripts_defaults == 1
		let g:vinscripts = s:defaults
	elseif type(g:vinscripts_defaults) == v:t_list
		for def in g:vinscripts_defaults
			let g:vinscripts[def] = s:defaults[def]
		endfor
	endif
endfunction

let &cpoptions = s:cpo_bck
