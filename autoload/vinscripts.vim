" Description:	Vinscripts core
" Maintainer:	DALECKI Léo
" License:	MIT License
" Home:		<https://framagit.org/Informancien/vinscripts>

let s:cpo_bck = &cpoptions
set cpoptions&vim

function! s:get_bin(embed_def)

	let bin = a:embed_def.bin

	" Select bin by encoding
	if exists('a:embed_def.bin_encoding')
		\ && has_key(a:embed_def.bin_encoding, &encoding)

		let bin = a:embed_def.bin_encoding[&encoding]
	endif

	" Check if bin is found
	call system('command -v ' . split(bin)[0])
	if v:shell_error != 0
		throw 'Cannot run ' . bin
	endif

	return bin

endfunction

function! vinscripts#preview()

	if len(g:vinscripts) == 0
		echo "Vinscripts isn't configured"
		return
	endif

	let cursor_bck = getcurpos() 
	let start_pos = 0

	for embed_def in values(g:vinscripts)
		
		let prev_pos = search('^' . embed_def.script_start, 'cbW')

		if prev_pos > start_pos
			let start_pos = prev_pos
			let script_def = embed_def
		endif
	endfor

	if start_pos != 0

		let bin = s:get_bin(script_def)
		let script_lines = getline(start_pos + 1, searchpair(
			\ '^' . script_def.script_start, '',
			\ '^' . script_def.script_end) - 1)
		let output = system(bin, script_lines)

		if v:shell_error != 0

			echohl ErrorMsg
			echom 'Script at line' start_pos 'with' bin
				\ ':'
			echom output
			echohl None
		else
			echo output
		endif
	endif

	call setpos('.', cursor_bck)

endfunction

function! vinscripts#render()

	if len(g:vinscripts) == 0
		echo "Vinscripts isn't configured"
		return
	endif

	let cursor_bck = getcurpos()
	let output_count = 0
	let output_errors = 0

	for embed_def in values(g:vinscripts)
	
		" Skip if filetype doesn't match
		if exists('embed_def.filetype') &&
			\ embed_def.filetype != expand('%:e')
			
			continue
		endif

		let bin = s:get_bin(embed_def)

		let output_before = exists('embed_def.output_before') &&
			\ embed_def.output_before == 1

		call cursor(1, 1)

		" Search for first script
		let script_start = search('^' . embed_def.script_start, 'cW')

		" Keep going until no more scripts are found
		while script_start != 0
			
			echo 'Script at line' script_start 'with' bin '... '

			let script_end = searchpair(
				\ '^' . embed_def.script_start, '',
				\ '^' . embed_def.script_end)
			let script_lines = getline(script_start + 1,
				\ script_end - 1)

			let output = systemlist(bin, script_lines)

			if v:shell_error != 0

				echohl ErrorMsg
				echon 'error'
				echom 'Script at line' script_start 'with' bin
					\ ':'

				for line in output
					echom line
				endfor
				
				echohl None

				let output_errors += 1
				let script_start = search(
					\ '^' . embed_def.script_start, 'W')

				continue
			endif
			
			call insert(output, embed_def.output_start)
			call add(output,  embed_def.output_end)

			call cursor(script_start, 1)

			if output_before
				" Previous output precedes script
				let output_pos = script_start - 1
				let old_start = searchpair(
					\ '^' . embed_def.output_start, '',
					\ '^' . embed_def.script_start, 'bW')

				" Put the new output before the old one
				if old_start != 0
					let output_pos = old_start - 1
				else
					" Append a line to output
					call add(output, '')
				endif
			else
				" Previous output follows script
				let output_pos = script_end + 1
				let old_start = searchpair(
					\ '^' . embed_def.script_start, '',
					\ '^' . embed_def.output_start, 'W')

				" Append line if script is at end of the file
				if output_pos >= line('$')
					call append(line('$'), '')
				endif
			endif

			" Add optionnal attribute to script start
			if exists('embed_def.script_attr')
				call setline(script_start,
					\ embed_def.script_start . ' hidden')
			endif

			let old_len = 0

			if old_start != 0

				call cursor(old_start, 1)

				" Search for old output's end
				let old_end = searchpair(embed_def.output_start,
					\ '', embed_def.output_end)
				let old_len = old_end - old_start

				silent execute old_start ',' old_end 'delete _'
			endif

			" Insert new output
			call append(output_pos, output)

			echon 'done'

			" Searching for next script after this one
			call cursor(script_end - old_len + len(output), 1)

			let output_count += 1 
			let script_start = search('^' . embed_def.script_start,
				\ 'W')
		endwhile
	endfor

	echom output_count 'output(s) inserted,' output_errors
		\ 'error(s) (see :messages)'

	call setpos('.', cursor_bck)

endfunction

let &cpoptions = s:cpo_bck
