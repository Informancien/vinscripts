" Description:	Vinscripts main plugin file
" Maintainer:	DALECKI Léo
" License:	MIT License
" Home:		<https://framagit.org/Informancien/vinscripts>

if exists('g:loaded_vinscripts')
	finish
endif
let g:loaded_vinscripts = 1

let s:cpo_bck = &cpoptions
set cpoptions&vim

if !exists('g:vinscripts')
	let g:vinscripts = {}
endif

if exists('g:vinscripts_defaults')
	call vinscripts#conf#defaults()
endif

command! VinscriptsPreview call vinscripts#preview()
command! VinscriptsRender call vinscripts#render()

let &cpoptions = s:cpo_bck
