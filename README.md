# Vinscripts

Vim Infix Scripts, a Vim plugin to insert the output of embed scripts in plain
text files:
* Make interactive programming documentation, insert scripts in plain text
  files and get their output next to them, live;
* Easily add visual aid to your [Vimwiki](https://github.com/vimwiki/vimwiki)
  with simple flow charts and diagrams via DOT or [Graph::Easy](http://www.bloodgate.com/perl/graph/manual/index.html)
  scripts rendered by graph-easy.

![Vinscripts demo](vinscripts_demo.gif)

## Installation

This plugin can be installed as part of a Vim package:

```
mkdir -p ~/.vim/pack/plugins/start/vinscripts # 'plugins' can be replaced if needed
git clone https://framagit.org/Informancien/vinscripts ~/.vim/pack/plugins/start/vinscripts
```

## Commands

* `:VinscriptsPreview`: Preview the script under the cursor;
* `:VinscriptsRender`: Render all scripts in the current file.

## Configuration

Embed scripts are configured by adding entries to the `g:vinscripts` dictionnary
in your `~/.vimrc`. Each entry of this dictionnary must be a nested dictionnary,
with the following keys:
* `script_start`: Opening script block delimiter;
* `script_end`: Script end block delimiter;
* `script_attr`: Optional, attributes to append to the opening script block
  delimiter once rendered (useful to hide the script once rendered);
* `output_start`: Opening output block delimiter;
* `output_end`: Output end block delimiter;
* `output_before`: Optional, set to `1` to insert output before the script;
* `bin`: Command or path to the script interpreter, the script will be sent to
  standard input, options can be given;
* `bin_encoding`: Optional, dictionnary of commands to handle different
  file encodings, for each pair, its key is a legal value of the `encoding` Vim
  option, and its value will replace `bin` when the encoding matches;
* `filetype`: Optional, only process scripts when the current file has this
  extension.

For example, here is how Graph::Easy scripts for Vimwiki are configured by
default:
```
let g:vinscripts = {}

let g:vinscripts.vimwiki_graph-easy = {
	\ 'script_start': '{{{graph-easy', " Delimiter preceding DOT scripts
	\ 'script_end': '}}}', " Delimiter following DOT scripts
	\ 'script_attr': 'hidden', " Appended to the script's opening delimiter
	\ 'output_start': '{{{graph', " Delimiter preceding the output
	\ 'output_end': '}}}', " Delimiter following the output
	\ 'output_before': 1, " Insert output before script
	\ 'bin': 'graph-easy --from_txt', " Scripts will be fed to graph-easy on stdin
	\ 'bin_encoding':
		\ {'utf-8': 'graph-easy --from_txt --boxart'}, " Boxart on UTF-8
	\ 'filetype': 'wiki' } " Will only apply on .wiki files
```

### Default script definitions

Vinscripts comes with defaults definitions that can be enabled by setting the
`g:vinscripts_defaults` variable in `~/.vimrc`. Its value can either be `1` to
enable all the default definitions, or it can be set to a list, of which each
item is a definition to enable:
* `vimwiki_dot`: DOT scripts contained in preformatted text blocks in Vimwiki,
  requires the `graph-easy` command to be available in a directory of `$PATH`;
* `vimwiki_graph-easy`: Graph::Easy scripts contained by preformatted text
  blocks in Vimwiki, better suited for ASCII graphs than DOT, requires the
  `graph-easy` command to be available in a directory of `$PATH`.

Example:
```
g:vinscripts_defaults = 1 " Enables all default definitions listed
above g:vinscripts_defaults = ['vimwiki_graph-easy'] " Only Graph::Easy in Vimwiki
```

## License

Vinscripts is developed by DALECKI Léo and provided under the MIT License (also
known as the Expat License).
